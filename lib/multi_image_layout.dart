library multi_image_layout;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:readmore/readmore.dart';
import 'dart:io';
import 'package:uuid/uuid.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'dart:typed_data';
import 'package:permission_handler/permission_handler.dart';
import 'package:screenshot/screenshot.dart';

class MultiImageViewer extends StatelessWidget {
   MultiImageViewer({
    Key? key,
    required this.images,
    required this.urls,
    required this.ids,
     this.size,
    this.fileTodelte,
    this.fit,
    this.tap = false,
    this.viewOne = false,
    this.index,
    this.onTapFun,
    this.captions,
    this.backgroundColor = Colors.black87,
    this.textStyle = const TextStyle(
      fontSize: 30,
    ),
    this.height = 205,
    this.width,
  }) : super(key: key);
  final BoxFit? fit;
  /// Color of the background image.
  final Color backgroundColor;

  ///Color for the textStyle
  final TextStyle textStyle;

  /// List of network images to display.
  final List<String> images;
  final List<String> urls;
  final List<int> ids;

  /// List of captions to display.
  ///
  /// Each caption is displayed with respect to its corresponding image.
  ///
  /// The number of captions `must` be equal to the number of images or else, captions will not be displayed.
  final List<String>? captions;
  final bool tap , viewOne;
  final int? index;
  final Function? onTapFun;
  final List<int>? size;
  final Function(String)? fileTodelte;

  /// Height of the image(s).
  ///
  /// If not set, it will be a height of 205.0.
  final double height;

  /// Width of the image(s).
  final double? width;

  @override
  Widget build(BuildContext context) {
    bool cache = false;
    final tempFile = File(images[viewOne ? index! : 0]);
    if(tempFile.existsSync()) {
      final tempFileSize = tempFile.lengthSync();
        if (size != null && size!.isNotEmpty &&
            size![viewOne ? index! : 0] != null) {
          if (tempFileSize != size![viewOne ? index! : 0]!) {
            debugPrint("Load from network for not equal size");

            if (images![viewOne ? index! : 0] != null) {
              if (fileTodelte != null) {
                debugPrint("call function to delete");

                fileTodelte!(images[viewOne ? index! : 0]!);

              }
            }
            else {
              debugPrint("not provided image index");

            }
          }
          else {
            cache = true;
            debugPrint("Load from cache for equal media size");

          }
        }
        else {
          debugPrint("Load from cache for non provided media size");
          cache = true;
        }
      }
    else {
      debugPrint("Load from network for non exist file");

    }



    /// MediaQuery Width
    double defaultWidth = MediaQuery.of(context).size.width;
    var uuid = const Uuid().v4();

    if (images.isEmpty) {
      return Container();
    }
    if (images.length == 1 || viewOne) {
      return GestureDetector(
        onTap: () {
          openImage(context,viewOne ? index! : 0, images, urls, captions,ids,uuid);
          },
        child: Hero(
          tag: "$uuid${ids[viewOne ? index! : 0]}",
          child: Container(
            height: height,
            width: width ?? defaultWidth,
            decoration: BoxDecoration(
              color: backgroundColor,
              image: DecorationImage(
                  image: (cache ?
                  FileImage(File(images[viewOne ? index! : 0])) :
                  NetworkImage(urls[viewOne ? index! : 0]) as ImageProvider), fit: fit ?? BoxFit.fill),
              borderRadius: const BorderRadius.all(
                Radius.circular(5),
              ),
            ),
          ),
        ),
      );
    } else if (images.length == 2) {
      return SizedBox(
        height: height,
        width: width ?? defaultWidth,
        child: Row(children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(right: 1),
              child: GestureDetector(
                onTap: tap ? ()=> onTapFun!() : () => openImage(context, 0, images, urls, captions,ids,uuid),
                child: Hero(
                  tag: "$uuid${ids[0]}",
                  child: Container(
                    height: height,
                    width: width == null ? defaultWidth / 2 : width! / 2,
                    decoration: BoxDecoration(
                        color: backgroundColor,
                        image: DecorationImage(
                            image: (File(images[0]).existsSync() ? FileImage(File(images[0])) : NetworkImage(urls[0]) as ImageProvider), fit: BoxFit.cover),
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(5),
                            bottomLeft: Radius.circular(5))),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 1),
              child: GestureDetector(
                onTap: tap ? ()=> onTapFun!() : () => openImage(context, 1, images, urls, captions,ids,uuid),
                child: Hero(
                  tag: "$uuid${ids[1]}",
                  child: Container(
                    height: height,
                    width: width == null ? defaultWidth / 2 : width! / 2,
                    decoration: BoxDecoration(
                        color: backgroundColor,
                        image: DecorationImage(
                            image: (File(images[1]).existsSync() ? FileImage(File(images[1])) : NetworkImage(urls[1]) as ImageProvider), fit: BoxFit.cover),
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(5),
                            bottomRight: Radius.circular(5))),
                  ),
                ),
              ),
            ),
          ),
        ]),
      );
    } else if (images.length == 3) {
      return SizedBox(
        height: height,
        width: width ?? defaultWidth,
        child: Row(children: [
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 2, bottom: 2),
                    child: GestureDetector(
                      onTap: tap ? ()=> onTapFun!() : () => openImage(context, 0, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[0]}",
                        child: Container(
                          decoration: BoxDecoration(
                              color: backgroundColor,
                              image: DecorationImage(
                                  image: (File(images[0]).existsSync() ? FileImage(File(images[0])) : NetworkImage(urls[0]) as ImageProvider),
                                  fit: BoxFit.cover),
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(5))),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 2),
                    child: GestureDetector(
                      onTap:  tap ? ()=> onTapFun!() : () => openImage(context, 1, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[1]}",
                        child: Container(
                          width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                              color: backgroundColor,
                              image: DecorationImage(
                                  image: (File(images[1]).existsSync() ? FileImage(File(images[1])) : NetworkImage(urls[1]) as ImageProvider),
                                  fit: BoxFit.cover),
                              borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(5))),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 0),
              child: GestureDetector(
                onTap:  tap ? ()=> onTapFun!() : () => openImage(context, 2, images, urls, captions,ids,uuid),
                child: Hero(
                  tag: "$uuid${ids[2]}",
                  child: Container(
                    width: width == null ? defaultWidth / 2 : width! / 2,
                    decoration: BoxDecoration(
                        color: backgroundColor,
                        image: DecorationImage(
                            image: (File(images[2]).existsSync() ? FileImage(File(images[2])) : NetworkImage(urls[2]) as ImageProvider), fit: BoxFit.cover),
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(5),
                            bottomRight: Radius.circular(5))),
                  ),
                ),
              ),
            ),
          ),
        ]),
      );
    } else if (images.length == 4) {
      return SizedBox(
        height: height,
        width: width ?? defaultWidth,
        child: Row(children: [
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 2, bottom: 2),
                    child: GestureDetector(
                      onTap: tap ? ()=> onTapFun!() : () => openImage(context, 0, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[0]}",
                        child: Container(
                          // width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                              color: backgroundColor,
                              image: DecorationImage(
                                  image: (File(images[0]).existsSync() ? FileImage(File(images[0])) : NetworkImage(urls[0]) as ImageProvider),
                                  fit: BoxFit.cover),
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(5))),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 2),
                    child: GestureDetector(
                      onTap: () =>  tap ? onTapFun!() : openImage(context, 1, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[1]}",
                        child: Container(
                          width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                              color: backgroundColor,
                              image: DecorationImage(
                                  image: (File(images[1]).existsSync() ? FileImage(File(images[1])) : NetworkImage(urls[1]) as ImageProvider),
                                  fit: BoxFit.cover),
                              borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(5))),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0, bottom: 2),
                    child: GestureDetector(
                      onTap: () =>  tap ? onTapFun!() :  openImage(context, 2, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[2]}",
                        child: Container(
                          width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                              color: backgroundColor,
                              image: DecorationImage(
                                  image: (File(images[2]).existsSync() ? FileImage(File(images[2])) : NetworkImage(urls[2]) as ImageProvider),
                                  fit: BoxFit.cover),
                              borderRadius: const BorderRadius.only(
                                  topRight: Radius.circular(5))),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0, top: 0),
                    child: GestureDetector(
                      onTap: () =>  tap ? onTapFun!() : openImage(context, 3, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[3]}",
                        child: Container(
                          width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                              color: backgroundColor,
                              image: DecorationImage(
                                  image: (File(images[3]).existsSync() ? FileImage(File(images[3])) : NetworkImage(urls[3]) as ImageProvider),
                                  fit: BoxFit.cover),
                              borderRadius: const BorderRadius.only(
                                  bottomRight: Radius.circular(5))),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]),
      );
    } else if (images.length > 4) {
      return SizedBox(
        height: height,
        width: width ?? defaultWidth,
        child: Row(children: [
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 2, bottom: 2),
                    child: GestureDetector(
                      onTap: tap ? ()=> onTapFun!() : () => openImage(context, 0, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[0]}",
                        child: Container(
                          decoration: BoxDecoration(
                              color: backgroundColor,
                              image: DecorationImage(
                                  image: (File(images[0]).existsSync() ? FileImage(File(images[0])) : NetworkImage(urls[0]) as ImageProvider),
                                  fit: BoxFit.cover),
                              borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(5))),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(right: 2),
                    child: GestureDetector(
                      onTap: () =>  tap ? onTapFun!() : openImage(context, 1, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[1]}",
                        child: Container(
                          width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                            color: backgroundColor,
                            image: DecorationImage(
                                image: (File(images[1]).existsSync() ? FileImage(File(images[1])) : NetworkImage(urls[1]) as ImageProvider),
                                fit: BoxFit.cover),
                            borderRadius: const BorderRadius.only(
                              bottomLeft: Radius.circular(5),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0, bottom: 2),
                    child: GestureDetector(
                      onTap: tap ? ()=> onTapFun!() : () => openImage(context, 2, images, urls, captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[2]}",
                        child: Container(
                          width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                            color: backgroundColor,
                            image: DecorationImage(
                                image: (File(images[2]).existsSync() ? FileImage(File(images[2])) : NetworkImage(urls[2]) as ImageProvider),
                                fit: BoxFit.cover),
                            borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(5),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 0, top: 0),
                    child: GestureDetector(
                      onTap: () =>  tap ? onTapFun!() : openImage(context, 3, images,urls ,captions,ids,uuid),
                      child: Hero(
                        tag: "$uuid${ids[3]}",
                        child: Container(
                          width: width == null ? defaultWidth / 2 : width! / 2,
                          decoration: BoxDecoration(
                            color: backgroundColor,
                            image: DecorationImage(
                                image: (File(images[3]).existsSync() ? FileImage(File(images[3])) : NetworkImage(urls[3]) as ImageProvider),
                                fit: BoxFit.cover),
                            borderRadius: const BorderRadius.only(
                              bottomRight: Radius.circular(5),
                            ),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.5),
                              borderRadius: const BorderRadius.only(
                                bottomRight: Radius.circular(5),
                              ),
                            ),
                            child: Center(
                                child: Text("+${images.length - 4}",
                                    style: textStyle)),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]),
      );
    } else {
      return const SizedBox();
    }
  }
}

/// View Image(s)
void openImage(BuildContext context, final int index, List<String> unitImages,
    List<String>? urls, List<String>? captions , List<int>? ids , dynamic uuid) {
  Navigator.push(
    context,
    MaterialPageRoute(
      builder: (context) => GalleryPhotoViewWrapper(
        galleryItems: unitImages,
        urls: urls,
        ids: ids,
        uuid: uuid,
        captions: captions,
        backgroundDecoration: const BoxDecoration(
          color: Colors.black,
        ),
        initialIndex: index,
        scrollDirection: Axis.horizontal,
      ),
    ),
  );
}
class GalleryPhotoViewWrapper extends StatefulWidget {
  GalleryPhotoViewWrapper({
    Key? key,
    this.loadingBuilder,
    this.backgroundDecoration,
    this.minScale,
    this.maxScale,
    this.initialIndex,
    required this.galleryItems,
    this.urls,
    this.captions,
    this.ids,
    this.uuid,
    this.scrollDirection = Axis.horizontal,
  })  : pageController = PageController(initialPage: initialIndex!),
        super(key: key);

  final BoxDecoration? backgroundDecoration;
  final List<String>? galleryItems;
  final List<String>? urls;
  final List<String>? captions;
  final List<int>? ids;
  final int? initialIndex;
  final LoadingBuilder? loadingBuilder;
  final dynamic maxScale;
  final dynamic minScale;
  final PageController pageController;
  final Axis scrollDirection;
  final dynamic uuid;

  @override
  State<StatefulWidget> createState() {
    return _GalleryPhotoViewWrapperState();
  }
}

class _GalleryPhotoViewWrapperState extends State<GalleryPhotoViewWrapper> {
  int? currentIndex;
  bool showCaptions = false;
  bool showOptions = true;
  ScreenshotController screenshotController =  ScreenshotController();


  @override
  void initState() {
    checkCaptionLength();
    currentIndex = widget.initialIndex;

    super.initState();
  }

  void checkCaptionLength() {
    if (widget.captions != null &&
        widget.captions!.length == widget.galleryItems!.length) {
      showCaptions = true;
    }
  }

  void onPageChanged(int index) {
    if (mounted) {
      setState(() {
        currentIndex = index;
      });
    }
  }
  void onShowOption() {
      setState(() {
        showOptions = !showOptions;
      });

  }

  PhotoViewGalleryPageOptions _buildItem(BuildContext context, int index) {
    final String item = widget.galleryItems![index];
    return PhotoViewGalleryPageOptions(
      imageProvider:( File(item).existsSync() ? FileImage(File(item)) : NetworkImage(widget.urls![index])) as ImageProvider,
      initialScale: PhotoViewComputedScale.contained,
      minScale: PhotoViewComputedScale.contained * (0.5 + index / 10),
      maxScale: PhotoViewComputedScale.covered * 4.1,
      heroAttributes: PhotoViewHeroAttributes(tag: "${widget.uuid}${widget.ids![index]}"),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 15, 41, 7),
      body: InkWell(
        onTap: () {
          onShowOption();
        },
        child: Container(
          decoration: widget.backgroundDecoration ??
              const BoxDecoration(
                color: Color.fromARGB(255, 13, 39, 6),
              ),
          constraints: BoxConstraints.expand(
            height: MediaQuery.of(context).size.height,
          ),
          child: Stack(
            alignment: showCaptions ? isTextRtl(widget.captions![currentIndex!]) ? Alignment.bottomRight : Alignment.bottomLeft : Alignment.bottomRight,
            children: <Widget>[
              Screenshot(
                controller: screenshotController,
                child: PhotoViewGallery.builder(
                  scrollPhysics: const BouncingScrollPhysics(),
                  builder: _buildItem,
                  itemCount: widget.galleryItems!.length,
                  loadingBuilder: widget.loadingBuilder,
                  backgroundDecoration: widget.backgroundDecoration ??
                      const BoxDecoration(
                        color: Color.fromARGB(255, 9, 40, 1),
                      ),
                  pageController: widget.pageController,
                  onPageChanged: onPageChanged,
                  scrollDirection: widget.scrollDirection,
                ),
              ),

              showOptions ?  widget.captions != null && widget.captions!.isNotEmpty ? Container(
                padding: const EdgeInsets.all(20.0),
                color: widget.captions![currentIndex!] != "" ? Colors.black.withOpacity(0.5):null,
                width: widget.captions![currentIndex!] != "" ? double.infinity : null,
                height: widget.captions![currentIndex!] != "" ?MediaQuery.of(context).size.height / 3.5 : null, // Set the desired height
                child: showCaptions
                    ? Directionality(
                  textDirection: isTextRtl(widget.captions![currentIndex!]) ? TextDirection.rtl : TextDirection.ltr,
                  child: SingleChildScrollView(
                    child: ReadMoreText(
                            widget.captions![currentIndex!],
                            trimLines: 5,
                            // textAlign: TextAlign.left,
                            style:  TextStyle(
                              color: Colors.white,
                              fontSize: 18.sp,
                            ),
                            colorClickableText: Colors.white,
                            trimMode: TrimMode.Line,
                            trimCollapsedText: 'Show more',
                            trimExpandedText: 'Show less',
                            moreStyle: const TextStyle(
                                fontSize: 17.0, fontWeight: FontWeight.bold),
                          ),
                  ),
                    )
                    : Text(
                        "Image ${currentIndex! + 1}",
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 17.0,
                          decoration: null,
                        ),
                      ),
              ) :SizedBox.shrink():SizedBox.shrink(),
              showOptions ? Positioned(
                top: 80,
                left: 15,
                child: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: IconButton(
                    constraints: const BoxConstraints(),
                    padding: EdgeInsets.zero,
                    icon: const Icon(Icons.close),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
              ) : SizedBox.shrink(),
              showOptions ? Positioned(
                top: 80,
                right: 15,
                child: Container(
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.5),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: IconButton(
                    constraints: const BoxConstraints(),
                    padding: EdgeInsets.zero,
                    icon: const Icon(Icons.download),
                    onPressed: () async {
                       saveToGallery(context);
                      // debugPrint(widget.urls![currentIndex!]);
                      // final status = await GallerySaver.saveImage(
                      //     '${widget.urls![currentIndex!]}.png');
                      // if (status == true) {
                      //   ScaffoldMessenger.of(context).showSnackBar(
                      //     const SnackBar(
                      //       content: Text("Image saved to gallery"),
                      //     ),
                      //   );
                      // } else {
                      //   ScaffoldMessenger.of(context).showSnackBar(
                      //     const SnackBar(
                      //       content: Text("Error saving image"),
                      //     ),
                      //   );
                      // }
                    },
                  ),
                ),
              ) : SizedBox.shrink()
            ],
          ),
        ),
      ),
    );
  }
  
  saveToGallery(BuildContext context) {

    screenshotController.capture().then((Uint8List? image) {
      if (image != null) {
        saveImage(image);
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Image saved to gallery.'),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('Error saving image'),
          ),
        );
        // Handle the case where image is null
      }
    }).catchError((err) => print(err));
  }
  saveImage(Uint8List bytes) async {
    final time = DateTime.now()
        .toIso8601String()
        .replaceAll('.', '-')
        .replaceAll(':', '-');
    final name = "screenshot_$time";
    await requestPermission(Permission.storage);
    await ImageGallerySaver.saveImage(bytes, name: name);
  }
    Future<bool> requestPermission(Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == PermissionStatus.granted) {
        return true;
      }
    }
    return false;
  }
  isTextRtl(String str) {
    if (str.isEmpty) return false;
    var arabic = RegExp(r'^[\u0621-\u064A]+');
    return arabic.hasMatch(getFirstNonSymbolChar(str));
  }
  String getFirstNonSymbolChar(String text) {
    for (int i = 0; i < text.length; i++) {
      String char = text[i];
      if (!RegExp(r'[!@#$%^&*()_+{}\[\]:;<>,.?~\\]').hasMatch(char)) {
        return char;
      }
    }
    // If no non-symbol character is found.
    return text[0];
  }
}
